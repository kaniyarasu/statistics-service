#!/usr/bin/env bash

cd $(dirname $0)

build() {
  mvn clean install
  true
}

run() {
  java -jar target/statistics-service-*.jar
  sleep 600
}

usage() {
  cat <<EOF
Usage:
  $0 <command>
Local machine commands:
  build        : builds and packages your app
  run          : starts your app in the foreground
EOF
}

action=$1
action=${action:-"usage"}
action=${action/help/usage}
shift
if type -t $action >/dev/null; then
  echo "Invoking: $action"
  $action $*
else
  echo "Unknown action: $action"
  usage
  exit 1
fi