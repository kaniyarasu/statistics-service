package com.n26.statistics.helper;

import com.n26.statistics.model.Transaction;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class TransactionValidator {

    @Value("${transaction.time.threshold:60}")
    private int transactionTimeThreshold;

    /**
     * Takes transaction and valid its timestamp
     * Note:- Should be changed to common validators with chain of responsibility which can enable multiple validation
     * @param transaction - Transaction to be validated
     * @return Boolean
     */
    public Boolean isTransactionValid(Transaction transaction) {
        long currentTimeInMillis = DateUtil.getCurrentTimeInMillis();

        if(transaction.getTimestamp() <= (currentTimeInMillis - transactionTimeThreshold * 1000)){
            return Boolean.FALSE;
        }

        return Boolean.TRUE;
    }
}
