package com.n26.statistics.helper;

import java.util.Calendar;
import java.util.TimeZone;

public class DateUtil {

    private DateUtil() {}

    public static long getCurrentTimeInMillis()
    {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        return cal.getTimeInMillis();
    }
}
