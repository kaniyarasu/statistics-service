package com.n26.statistics.action.transaction;

import com.n26.statistics.model.Transaction;
import com.n26.statistics.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GetTransactionAction {

    @Autowired
    private TransactionRepository transactionRepository;

    public List<Transaction> invoke() {
        return transactionRepository.findAll();
    }
 }
