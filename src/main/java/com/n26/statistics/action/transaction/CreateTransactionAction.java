package com.n26.statistics.action.transaction;

import com.n26.statistics.event.Event;
import com.n26.statistics.event.EventHandler;
import com.n26.statistics.exception.ErrorType;
import com.n26.statistics.exception.InvalidDataException;
import com.n26.statistics.helper.TransactionValidator;
import com.n26.statistics.model.Transaction;
import com.n26.statistics.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CreateTransactionAction {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private EventHandler eventHandler;

    @Autowired
    private TransactionValidator transactionValidator;

    /**
     * Valid the input transaction, if valid persists transaction
     * If not valid, throws InvalidDataException
     * @param transaction - Transaction to be created
     */
    public void invoke(Transaction transaction) {
        if(transactionValidator.isTransactionValid(transaction)) {
            transactionRepository.createTransaction(transaction);

            //Didn't added event publisher, using handler directly
            eventHandler.handle(Event.TRANSACTION_CREATED);
        } else {
            throw new InvalidDataException(ErrorType.OLDER_TRANSACTION);
        }
    }
}
