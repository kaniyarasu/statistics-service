package com.n26.statistics.action.statistics;

import com.n26.statistics.model.Statistics;
import com.n26.statistics.repository.StatisticsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GetStatisticsAction {

    @Autowired
    private StatisticsRepository statisticsRepository;

    public Statistics invoke() {
        return statisticsRepository.getStatistics();
    }
}
