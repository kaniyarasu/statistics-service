package com.n26.statistics.action.statistics;

import com.n26.statistics.action.transaction.GetTransactionAction;
import com.n26.statistics.model.Statistics;
import com.n26.statistics.model.Transaction;
import com.n26.statistics.repository.StatisticsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.DoubleSummaryStatistics;
import java.util.List;

@Component
public class GenerateStatisticsAction {

    @Autowired
    private StatisticsRepository statisticsRepository;

    @Autowired
    private GetTransactionAction getTransactionAction;

    public void invoke() {
        statisticsRepository.updateStatistics(generate());
    }

    private Statistics generate() {
        Statistics statistics = new Statistics();

        //Read all transactions
        List<Transaction> transactions = getTransactionAction.invoke();

        //Calculate the statistics
        DoubleSummaryStatistics doubleSummaryStatistics = transactions.stream()
                .mapToDouble(Transaction::getAmount)
                .summaryStatistics();

        //Propagate the statistics
        statistics.setCount(doubleSummaryStatistics.getCount());
        statistics.setSum(doubleSummaryStatistics.getSum());
        statistics.setAvg(doubleSummaryStatistics.getAverage());
        statistics.setMax(doubleSummaryStatistics.getMax());
        statistics.setMin(doubleSummaryStatistics.getMin());

        return statistics;
    }
}
