package com.n26.statistics.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.ResourceBundle;

@Getter
@ResponseStatus(value= HttpStatus.NO_CONTENT)
public class InvalidDataException extends RuntimeException {

    private final ErrorType errorType;
    private final String message;
    private final transient Object[] arguments;

    public InvalidDataException(ErrorType errorType, Object... arguments) {
        this.errorType = errorType;
        this.arguments = arguments;
        this.message = String.format(
                ResourceBundle.getBundle("errors").getString(errorType.getValue()),
                arguments);
    }

    public InvalidDataException(ErrorType errorType) {
        this.errorType = errorType;
        this.arguments = new Object[0];
        this.message = ResourceBundle.getBundle("errors").getString(errorType.getValue());
    }
}
