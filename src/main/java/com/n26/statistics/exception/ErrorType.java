package com.n26.statistics.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ErrorType
{
    OLDER_TRANSACTION("EC_0");

    private String value;
}
