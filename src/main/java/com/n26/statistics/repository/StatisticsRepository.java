package com.n26.statistics.repository;

import com.n26.statistics.model.Statistics;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Holds the Statistics data and provides API to update and access the statistics
 * Right now supports only persistence single Statistics, and can be extended to support multiple
 */
@Repository
public class StatisticsRepository {
    private final Statistics statistics;

    @Autowired
    private ModelMapper modelMapper;

    public StatisticsRepository() {
        statistics = new Statistics();
    }

    /**
     * Updates the statistics record
     * @param statistics - Updated record
     */
    public void updateStatistics(Statistics statistics) {
        //Copying the new stats to this main record
        modelMapper.map(statistics, this.statistics);
    }

    /**
     * Return the current statistics
     * @return Statistics
     */
    public Statistics getStatistics() {
        //Sending back cloned object to prevent modification on the main record
        return modelMapper.map(this.statistics, Statistics.class);
    }
}
