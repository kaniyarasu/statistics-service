package com.n26.statistics.repository;

import com.n26.statistics.event.Event;
import com.n26.statistics.event.EventHandler;
import com.n26.statistics.helper.TransactionValidator;
import com.n26.statistics.model.Transaction;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
@Slf4j
public class TransactionRepository {

    /**
     * Using LinkedList for the following reason
     *
     * Manipulation of data(Iteration) as main functionality
     * Insertion at end
     * Deletion with iteration
     * We don't need position based access
     */
    private final LinkedList<Transaction> transactions;

    @Autowired
    private TransactionValidator transactionValidator;

    @Autowired
    private EventHandler eventHandler;

    @Autowired
    private ModelMapper modelMapper;

    public TransactionRepository() {
        this.transactions = new LinkedList<>();
    }

    /**
     * Adds the given transaction to the data structure
     * @param transaction - Transaction to be created
     */
    public void createTransaction(Transaction transaction) {
        transactions.addLast(transaction);
    }

    /**
     * Return all the non-expired transactions.
     * @return List<Transaction>
     */
    public List<Transaction> findAll() {
        //Cloning to new list to prevent modifications to existing list.
        return transactions
                .stream()
                .map(transaction -> modelMapper.map(transaction, Transaction.class))
                .collect(Collectors.toList());
    }

    /**
     * Delete all the transactions, using only for testing!!
     */
    public void deleteAll() {
        transactions.clear();
    }

    /**
     * Polls the transactions every second, and removes expired transactions
     */
    @Scheduled(fixedDelayString = "${transaction.cleaner.frequency:1000}")
    private void cleanup() {
        log.debug("Cleanup started");
        boolean transactionsExpired = transactions
                .removeIf(transaction -> !transactionValidator.isTransactionValid(transaction));

        if(transactionsExpired) {
            eventHandler.handle(Event.TRANSACTION_EXPIRED);
        }
        log.debug("Cleanup completed");
    }
}
