package com.n26.statistics.controller;

import com.n26.statistics.action.transaction.CreateTransactionAction;
import com.n26.statistics.model.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/transactions")
public class TransactionController {

    @Autowired
    private CreateTransactionAction createTransactionAction;

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST, consumes = "application/json")
    public void postTransaction(@RequestBody @Valid Transaction transaction) {
        createTransactionAction.invoke(transaction);
    }
}
