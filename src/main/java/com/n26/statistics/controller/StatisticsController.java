package com.n26.statistics.controller;

import com.n26.statistics.action.statistics.GetStatisticsAction;
import com.n26.statistics.model.Statistics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(path = "/statistics")
public class StatisticsController {

    @Autowired
    private GetStatisticsAction getStatisticsAction;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public Statistics getStatistics() {
        return getStatisticsAction.invoke();
    }
}
