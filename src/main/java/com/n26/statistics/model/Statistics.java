package com.n26.statistics.model;

import lombok.Data;

@Data
public class Statistics {
    private Double sum;

    private Double avg;

    private Double max;

    private Double min;

    private Long count;
}
