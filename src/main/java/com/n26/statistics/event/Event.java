package com.n26.statistics.event;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Event {

    TRANSACTION_CREATED(100),
    TRANSACTION_EXPIRED(101);

    private Integer value;
}
