package com.n26.statistics.event;

import com.n26.statistics.action.statistics.GenerateStatisticsAction;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * Handles for events, and invokes the event specific action.
 * Future:- Handler should be extended to handle multiple event in generic way(Chain of responsibility)
 */
@Component
@Slf4j
public class EventHandler {

    @Autowired
    private GenerateStatisticsAction generateStatisticsAction;

    /**
     * Handles incoming event, and invokes the specific action
     * @param event - Incoming event
     */
    @Async("processExecutor")
    public void handle(Event event) {
        log.info("Event received: {}", event.getValue());
        if (event == Event.TRANSACTION_CREATED || event == Event.TRANSACTION_EXPIRED) {
            generateStatisticsAction.invoke();
        }
    }
}
