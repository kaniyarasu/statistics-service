package com.n26.statistics.helper;

import com.n26.statistics.model.Transaction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.assertFalse;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TransactionValidatorTest {

    @Autowired
    private TransactionValidator transactionValidator;

    @Test
    public void testValidTransaction() {
        Transaction transaction = prepareTransaction();
        assertTrue(transactionValidator.isTransactionValid(transaction));
    }

    @Test
    public void testInvalidTransaction() {
        Transaction transaction = prepareTransaction();
        transaction.setTimestamp(transaction.getTimestamp() - 100 * 1000);
        assertFalse(transactionValidator.isTransactionValid(transaction));
    }

    private Transaction prepareTransaction() {
        Transaction transaction = new Transaction();
        transaction.setAmount(50d);
        transaction.setTimestamp(DateUtil.getCurrentTimeInMillis());
        return transaction;
    }
}
