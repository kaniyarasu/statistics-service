package com.n26.statistics.repository;

import com.n26.statistics.helper.DateUtil;
import com.n26.statistics.model.Transaction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static junit.framework.Assert.assertEquals;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TransactionRepositoryTest {

    @Autowired
    private TransactionRepository transactionRepository;

    @Test
    public void testCreateAndGetTransaction() {
        Transaction transaction = prepareTransaction();
        
        transactionRepository.createTransaction(transaction);

        List<Transaction> fromDB = transactionRepository.findAll();
        assertEquals(1, fromDB.size());
        assertEquals(transaction.getAmount(), fromDB.get(0).getAmount());
        assertEquals(transaction.getTimestamp(), fromDB.get(0).getTimestamp());
    }

    @Test
    public void testTransactionExpiry() {
        //Should not use this, use Mocking!!
        transactionRepository.deleteAll();

        Transaction transaction = prepareTransaction();
        transaction.setTimestamp(transaction.getTimestamp() - 60 * 1000);
        transactionRepository.createTransaction(transaction);

        //Can be changed to await(), should be removed
        try {Thread.sleep(2000l);} catch (InterruptedException ignored) {}

        List<Transaction> fromDB = transactionRepository.findAll();
        assertEquals(0, fromDB.size());
    }

    private Transaction prepareTransaction() {
        Transaction transaction = new Transaction();
        transaction.setAmount(50d);
        transaction.setTimestamp(DateUtil.getCurrentTimeInMillis());
        return transaction;
    }
}
