package com.n26.statistics.repository;

import com.n26.statistics.model.Statistics;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static junit.framework.Assert.assertEquals;

@SpringBootTest
@RunWith(SpringRunner.class)
public class StatisticsRepositoryTest {

    @Autowired
    private StatisticsRepository statisticsRepository;

    @Test
    public void testUpdateAndGetStatistics() {
        Statistics statistics = prepareStatistics();
        statisticsRepository.updateStatistics(statistics);

        Statistics fromDB = statisticsRepository.getStatistics();
        assertEquals(statistics.getAvg(), fromDB.getAvg());
        assertEquals(statistics.getSum(), fromDB.getSum());
        assertEquals(statistics.getMax(), fromDB.getMax());
        assertEquals(statistics.getMin(), fromDB.getMin());
        assertEquals(statistics.getCount(), fromDB.getCount());
    }

    private Statistics prepareStatistics()
    {
        Statistics statistics = new Statistics();
        statistics.setAvg(10d);
        statistics.setSum(20d);
        statistics.setMax(15d);
        statistics.setMin(5d);
        statistics.setCount(2l);
        return statistics;
    }
}
