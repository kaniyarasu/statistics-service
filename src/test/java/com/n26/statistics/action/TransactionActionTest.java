package com.n26.statistics.action;

import com.n26.statistics.action.transaction.CreateTransactionAction;
import com.n26.statistics.action.transaction.GetTransactionAction;
import com.n26.statistics.exception.InvalidDataException;
import com.n26.statistics.helper.DateUtil;
import com.n26.statistics.model.Transaction;
import com.n26.statistics.repository.TransactionRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static junit.framework.Assert.assertEquals;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TransactionActionTest {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private CreateTransactionAction createTransactionAction;

    @Autowired
    private GetTransactionAction getTransactionAction;

    @Test
    public void testCreateValidTransaction() {
        transactionRepository.deleteAll();

        Transaction transaction = prepareTransaction();
        createTransactionAction.invoke(transaction);
        List<Transaction> fromDB = getTransactionAction.invoke();

        assertEquals(1, fromDB.size());
        assertEquals(transaction.getAmount(), fromDB.get(0).getAmount());
        assertEquals(transaction.getTimestamp(), fromDB.get(0).getTimestamp());
    }

    @Test(expected = InvalidDataException.class)
    public void testCreateInvalidTransaction() {
        Transaction transaction = prepareTransaction();
        transaction.setTimestamp(transaction.getTimestamp() - 100 * 1000);
        createTransactionAction.invoke(transaction);
    }

    private Transaction prepareTransaction() {
        Transaction transaction = new Transaction();
        transaction.setAmount(50d);
        transaction.setTimestamp(DateUtil.getCurrentTimeInMillis());
        return transaction;
    }
}
