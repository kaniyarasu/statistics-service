package com.n26.statistics.action;

import com.n26.statistics.action.statistics.GenerateStatisticsAction;
import com.n26.statistics.action.statistics.GetStatisticsAction;
import com.n26.statistics.action.transaction.CreateTransactionAction;
import com.n26.statistics.helper.DateUtil;
import com.n26.statistics.model.Statistics;
import com.n26.statistics.model.Transaction;
import com.n26.statistics.repository.TransactionRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static junit.framework.Assert.assertEquals;

@SpringBootTest
@RunWith(SpringRunner.class)
public class StatisticsActionTest {

    @Autowired
    private GenerateStatisticsAction generateStatisticsAction;

    @Autowired
    private GetStatisticsAction getStatisticsAction;

    @Autowired
    private TransactionRepository transactionRepository;

    @Test
    public void testStatistics() {
        transactionRepository.deleteAll();
        transactionRepository.createTransaction(prepareTransaction(1));
        transactionRepository.createTransaction(prepareTransaction(2));

        generateStatisticsAction.invoke();

        Statistics statistics = getStatisticsAction.invoke();
        assertEquals(75d, statistics.getAvg());
        assertEquals(150d, statistics.getSum());
        assertEquals(100d, statistics.getMax());
        assertEquals(50d, statistics.getMin());
        assertEquals(new Long(2), statistics.getCount());
    }

    private Transaction prepareTransaction(int multiplier)
    {
        Transaction transaction = new Transaction();
        transaction.setAmount(multiplier * 50d);
        transaction.setTimestamp(DateUtil.getCurrentTimeInMillis());
        return transaction;
    }
}
